``A. The openFDA API is based on Elastic Search, could this API be replaced/enhanced with another technology/SaaS? ``

I would consider replacing Elastic Search with Apache Solr. Moreover, I see that dates very frequently appear in data of openFDA so I also would consider using InfluxDB.

``B. Which technology would you use to recreate such an API without using Elastic Search? ``
As backend technology, I would use: Java 8, Maven, Spring Boot / Data / Security, Swagger
For the database, I would choose one of them: MySQL, OracleDB, MongoDB, InfluxDB, Elastic Search
	
``C. How would you organize the implementation of this API (team organization, project management etc...)? ``

Firstly I would need information on how users mostly ask for the data.

Secondly, I would check how the database is organized to prepare the first specification of the REST API schema.

Thirdly, I would create JIRA /GITLAB tickers and assigned to developers tasks to create controllers, repositories and data objects. Then I would start testing.

The best would be if one developer would build controller, repository and data structure for one table and later we could decide on meetings which implementation is the most effective and which specification we should follow. For sure SCRUM/AGILE methodology, code reviews and using Sonarqube would be useful.

``D. How would you secure this endpoint, manage permissions/authorizations? ``

I would secure the endpoints with OAuth2 or SSO with SAML2. I would implement roles and privileges in Spring Security.

``E. What strategy would you adopt to monitor this API?``

Firstly, I would use Swagger to see REST API from “outside” and test it. 
Secondly, in Java, I would use library Slf4j to print logs and save them to Elastisearch
Thirdly, I would monitor the dashboard with Grafana and set alerts. 
