# Novartis UI

## Available Scripts

It is required to install node.js ( >10.16.0 ) before. https://nodejs.org/en/download/

In the project directory, you can run:

### `npm install`

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## 6 Tasks

`Task 1) Implement a Single Page Application (preferably with React) capable of fetching data from one of the adverse event endpoints (Animal & Veterinary, Drug, Device, Food), and display efficiently the result. There is no specific rule for UI/UX, creativity and simplicity are appreciated. Feel free to use any relevant UI library or starter kit to achieve this task.`

Done. Please check how page reacts on mobile devices.

`Task 2) Add one or two component’s (e.g.: Range Slider, Time Picker, Search Input) to your application, to change parameters in order to update User Interface with new data coming from the openFDA API. A well-organized application is appreciated, so feel free to use a Flux like State management library.`

Done. I used Redux. In Form.tsx there is InputRange component which allows changing limit.

`Task3) Integrate two visualizations to represent data coming from the API, you can get inspiration from the “Explore the API with an interactive chart” tab.`

Done. Please check v1 and v2 in dropdown menu

`Task4) Add a few Tests for your application to validate previously created features.`

Done in SmartTable.test.tsx and App.test.tsx

`Task5) Provide succinct documentation (e.g. to explain how to run locally the project) for your Application.`

Done. README.md file. If I have more time I would create a docker file to simplify running locally the project.

`Task6) (Optional) Deploy your application using the hosting solution of your choice (e.g. netlify, now, surge, S3 etc...) – Add a link of your hosted app in your repository.`

Done. http://novartis.michalrafalski.com/

## What is missing?

- [TS] I would like to replace some part of the code with React Hooks
- [SCSS] More styles for responsive design. It is OK, but it requires some polishing.
- [SCSS] Global variables for main colors
- [TS][scss] Documentation in the code
- [TS] Replace type `any` with real types
- [TS] Loading animation
- [DevOps] Add pipelines to bitbucket to upload directly to the server
- [DevOps] Docker file

## Author

Michal Rafalski https://michalrafalski.com
