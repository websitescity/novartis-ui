import * as React from "react";
import { RootState } from "../../store";
import { connect } from "react-redux";
import { pullData } from "../../store/actions";
import { StoreState } from "../../store/reducers";
import { ThunkDispatch } from "redux-thunk";
// Smart components
import SmartTable from "../smart/SmartTable/SmartTable";
import SmartDoughnut from "../smart/SmartDoughnut/SmartDoughnut";
import SmartGraph from "../smart/SmartGraph/SmartGraph";

import InputRange from "react-input-range";
import "./Form.scss";

interface State {
  url: string;
  limit: number;
  pageType: number;
}

interface OwnProps {}

interface DispatchProps {
  pullData: (username: string, password: string) => void;
}

interface StateProps {
  source: StoreState;
}

interface Page {
  name: string;
  url: string;
}

type Props = StateProps & OwnProps & DispatchProps;

class Form extends React.Component<Props, State> {
  private pages: Array<Page> = [
    {
      name: "Animal",
      url:
        "https://api.fda.gov/animalandveterinary/event.json?search=original_receive_date:[20040101+TO+20161107]"
    },
    {
      name: "Drug",
      url:
        "https://api.fda.gov/drug/event.json?search=receivedate:[20040101+TO+20081231]"
    },
    {
      name: "Devide",
      url:
        "https://api.fda.gov/device/event.json?search=date_received:[20130101+TO+20141231]"
    },
    {
      name: "Food",
      url:
        "https://api.fda.gov/food/enforcement.json?search=report_date:[20040101+TO+20131231]"
    },
    {
      name: "v1",
      url:
        "https://api.fda.gov/animalandveterinary/event.json?count=outcome.medical_status"
    },
    {
      name: "v2",
      url:
        "https://api.fda.gov/animalandveterinary/event.json?count=drug.manufacturing_date"
    }
  ];

  constructor(prop: Props) {
    super(prop);
    // Binding
    this.handleChange = this.handleChange.bind(this);
    this.handleLimitChange = this.handleLimitChange.bind(this);

    // Default state
    this.state = {
      url:
        "https://api.fda.gov/drug/event.json?search=receivedate:[20040101+TO+20081231]",
      limit: 10,
      pageType: 1
    };

    // Get data
    this.props.pullData(this.state.url, "&limit=" + this.state.limit);
  }

  handleChange(event: React.ChangeEvent<HTMLSelectElement>) {
    event.preventDefault();
    this.setState({ url: event.target.value });
    this.setState({ pageType: event.target.selectedIndex });

    //TODO: Only for prototype. Change algorithm.
    this.props.pullData(
      event.target.value,
      event.target.selectedIndex < 4 ? "&limit=" + this.state.limit : ""
    );
  }

  // TODO: Change type
  handleLimitChange(value: any) {
    this.setState({ limit: value });
    this.props.pullData(this.state.url, "&limit=" + value);
  }

  renderNavbar() {
    return (
      <nav className="navbar navbar-dark bg-dark loading">
        <span className="navbar-brand">Novartis UI</span>

        <span className="navbar-text col-5 col-md-7">
          {/* Only for table view */}
          {this.state.pageType < 4 && (
            <InputRange
              maxValue={100}
              minValue={0}
              value={this.state.limit}
              onChange={value => this.handleLimitChange(value)}
            />
          )}
        </span>
        <form className="form-inline">
          <select
            className="form-control"
            value={this.state.url}
            onChange={this.handleChange}
            disabled={this.props.source.isFetching}
          >
            {this.pages.map((page: Page, key: number) => {
              return (
                <option key={key} value={page.url}>
                  {page.name}
                </option>
              );
            })}
          </select>
        </form>
      </nav>
    );
  }
  render() {
    return (
      <React.Fragment>
        {this.renderNavbar()}
        <div className="row justify-content-center">
          <div className="col-8" />
        </div>
        <div className="row no-gutters">
          <div className="col-12">
            {/* TODO: Change to react router */}
            {this.state.pageType < 4 && (
              <SmartTable source={this.props.source.source} />
            )}
            {this.state.pageType === 4 && (
              <SmartDoughnut source={this.props.source.source} />
            )}
            {this.state.pageType === 5 && (
              <SmartGraph source={this.props.source.source} />
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (states: RootState, ownProps: OwnProps): StateProps => {
  return {
    source: states.session.source
  };
};

const mapDispatchToProps = (
  dispatch: ThunkDispatch<{}, {}, any>,
  ownProps: OwnProps
): DispatchProps => {
  return {
    pullData: async (url, limit) => {
      await dispatch(pullData(url, limit));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);
