import React, { Component } from "react";
import JSONTree from "react-json-tree";
import Popup from "reactjs-popup";

export interface SmartCellProps {
  cellValue: any;
  column: string;
}

class SmartCell extends Component<SmartCellProps> {
  treeTheme(): object {
    return {
      scheme: "monokai",
      author: "wimer hazenberg (http://www.monokai.nl)",
      base00: "#282c34",
      base01: "#383830",
      base02: "#49483e",
      base03: "#75715e",
      base04: "#a59f85",
      base05: "#f8f8f2",
      base06: "#f5f4f1",
      base07: "#f9f8f5",
      base08: "#f92672",
      base09: "#fd971f",
      base0A: "#f4bf75",
      base0B: "#a6e22e",
      base0C: "#a1efe4",
      base0D: "#66d9ef",
      base0E: "#ae81ff",
      base0F: "#cc6633"
    };
  }

  isObject(cellValue: any): boolean {
    return typeof cellValue === "object";
  }

  isString(cellValue: any): boolean {
    return typeof cellValue === "string";
  }

  isLongerString(cellValue: string): boolean {
    return cellValue.length > 200;
  }

  isDate(cellValue: string, column: string): boolean {
    return column.includes("date") && cellValue.length === 8;
  }

  printDate(cellValue: string): string {
    return (
      cellValue.slice(0, 4) +
      "-" +
      cellValue.slice(4, 6) +
      "-" +
      cellValue.slice(6)
    );
  }

  isBiggerObject(cellValue: object): boolean {
    return Object.keys(cellValue).length > 10;
  }

  printLongerText(cellValue: string): string {
    return "too long string";
  }

  printObject(cellValue: object): any {
    let wrapper;
    this.isBiggerObject(cellValue)
      ? (wrapper = {
          collection: cellValue
        })
      : (wrapper = cellValue);
    return (
      <React.Fragment>
        <JSONTree
          hideRoot
          data={wrapper}
          theme={this.treeTheme()}
          invertTheme={false}
        />
        <Popup
          trigger={<span className="previewIcon">&#128269;</span>}
          modal
          closeOnDocumentClick
        >
          <JSONTree
            hideRoot
            data={cellValue}
            theme={this.treeTheme()}
            invertTheme={false}
          />
        </Popup>
      </React.Fragment>
    );
  }

  render() {
    const cellValue: any = this.props.cellValue;
    const column: string = this.props.column;
    if (this.isObject(cellValue) && !this.isBiggerObject(cellValue)) {
      return <td data-title={column}>{this.printObject(cellValue)}</td>;
    } else if (this.isObject(cellValue) && this.isBiggerObject(cellValue)) {
      return <td data-title={column}>{this.printObject(cellValue)}</td>;
    } else if (this.isString(cellValue) && this.isLongerString(cellValue)) {
      return <td data-title={column}>{this.printLongerText(cellValue)}</td>;
    } else if (this.isString(cellValue) && this.isDate(cellValue, column)) {
      return <td data-title={column}>{this.printDate(cellValue)}</td>;
    } else {
      return <td data-title={column}>{cellValue}</td>;
    }
  }
}

export default SmartCell;
