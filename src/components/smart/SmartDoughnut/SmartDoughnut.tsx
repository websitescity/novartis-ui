import React, { Component } from "react";
import { Doughnut } from "react-chartjs-2";

import "./SmartDoughnut.scss";

export interface SmartDoughnutProps {
  source: any;
}

class SmartDoughnut extends Component<SmartDoughnutProps> {
  render() {
    let graphData: any = {
      labels: [],
      datasets: [
        {
          data: [],
          backgroundColor: [
            "#781c81",
            "#413992",
            "#447cbf",
            "#5ba7a6",
            "#83ba6d",
            "#b4bd4c",
            "#dbab3b",
            "#e7732f",
            "#d92120"
          ],
          hoverBackgroundColor: [
            "#781c81",
            "#413992",
            "#447cbf",
            "#5ba7a6",
            "#83ba6d",
            "#b4bd4c",
            "#dbab3b",
            "#e7732f",
            "#d92120"
          ]
        }
      ]
    };

    //TODO: Change type
    this.props.source.forEach((item: any) => {
      graphData.labels.push(item.term);
      graphData.datasets[0].data.push(item.count);
    });
    return (
      <div className="fullHeight">
        <Doughnut data={graphData} options={{ maintainAspectRatio: false }} />
      </div>
    );
  }
}

export default SmartDoughnut;
