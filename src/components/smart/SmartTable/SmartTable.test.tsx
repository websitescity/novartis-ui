// TODO Move to config file setupTests.ts
// setup file
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

import * as React from "react";

import { shallow } from "enzyme";
import SmartTable from "./SmartTable";
import SmartCell from "../SmartCell/SmartCell";
import renderer from "react-test-renderer";

describe("test table", () => {
  it("generate array of headers", () => {
    const wrapper: any = shallow(<SmartTable source={null} />);
    const jsonWithHeaders: any = [
      { second: null, first: null },
      { third: null }
    ];
    expect(wrapper.instance().getHeaders(jsonWithHeaders)).toStrictEqual([
      "first",
      "second",
      "third"
    ]);
  });

  it("component without data", () => {
    const wrapper: any = shallow(<SmartTable source={null} />);
    expect(wrapper.instance().renderTable()).toBe("No data");
  });

  it("component with sample data", () => {
    const sampleData = [
      {
        receiptdateformat: "102",
        receiver: null,
        companynumb: "1111",
        receivedateformat: "102",
        primarysource: null,
        seriousnessother: "1",
        transmissiondateformat: "102",
        fulfillexpeditecriteria: "1",
        safetyreportid: "4322505-4",
        sender: { senderorganization: "FDA-Public Use" },
        receivedate: "20040319",
        patient: {
          patientonsetage: "56",
          reaction: [
            { reactionmeddrapt: "a" },
            { reactionmeddrapt: "b" },
            { reactionmeddrapt: "c" }
          ],
          patientonsetageunit: "801",
          patientsex: "1",
          drug: [
            {
              drugstartdateformat: "102"
            },
            {
              drugcharacterization: "2",
              medicinalproduct: "aaa)"
            },
            { drugcharacterization: "2", medicinalproduct: "b" },
            {
              drugcharacterization: "2",
              openfda: {
                product_ndc: ["551-160", "71209-097"],
                package_ndc: ["139-602-05", "55111-1650"],
                generic_name: ["c", "d"],
                spl_set_id: [
                  "ef145e-56d8-4dea-a136-ec462b335641",
                  "c5481a-2321-453f-9469-dfcf709e2e2e"
                ],
                brand_name: [
                  "OFLOXAN OTIC",
                  "OFLACIN OPHTHALMIC",
                  "OFLACIN",
                  "m"
                ],
                manufacturer_name: ["Inc.", "Inc."],
                unii: ["A4P49JAZ9H"],
                rxcui: [
                  "207202",
                  "312075",
                  "198048",
                  "198049",
                  "198050",
                  "242446"
                ],
                spl_id: [
                  "509dde59-6f4d-410b-9835-81eb6a3e7623",
                  "8a21bee4-9f42-27bb-e053-2a95a90ada92"
                ],
                substance_name: ["OFLOXACIN"],
                product_type: ["HUMAN PRESCRIPTION DRUG"],
                route: ["AURICULAR (OTIC)", "ORAL", "OPHTHALMIC"],
                application_number: ["ANDA076407", "ANDA076830"]
              },
              medicinalproduct: "OFLOXACIN"
            }
          ]
        },
        seriousnesshospitalization: "1",
        transmissiondate: "20041129",
        serious: "1",
        receiptdate: "200403"
      }
    ];
    const tree = renderer.create(<SmartTable source={sampleData} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
