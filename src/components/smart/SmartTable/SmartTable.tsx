import React, { Component, Fragment } from "react";
// Assets & Style
import "./SmartTable.scss";
import SmartCell from "../SmartCell/SmartCell";

export interface SmartTableProps {
  source: any;
}

class SmartTable extends Component<SmartTableProps> {
  public getHeaders(source: any): Array<string>{
    let headers: Array<string> = [];
    console.log(JSON.stringify(source));
    for (let row of source) {
      for (let header in row) {
        if (!header.includes("dateformat")) {
          headers.push(header);
        }
      }
    }
    return Array.from(new Set(headers)).sort();
  }

  public renderTable() {
    if (!this.props.source) {
      return "No data";
    }
    // Get all headers
    let headers: Array<string> = this.getHeaders(this.props.source);

    return (
      <div id="no-more-tables" className="table-responsive table-scroll ">
        <table className="table table-hover table-striped table-sm table-dark">
          <thead>
            <tr>
              {headers.map((field: any, i) => {
                return (
                  <th key={i} scope="col">
                    {field.split("_").join("\n\r")}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {this.props.source.map((contact: any, index: any) => {
              return (
                <tr key={index}>
                  {headers.map((field: any, i) => {
                    let cellValue = contact[field];
                    if (
                      contact[field] &&
                      contact[field] !== null &&
                      contact[field] !== ""
                    ) {
                      return (
                        <SmartCell
                          key={i}
                          column={field}
                          cellValue={cellValue}
                        />
                      );
                    } else {
                      return (
                        <td key={i} data-title={field}>
                          -
                        </td>
                      );
                    }
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }

  render() {
    return <Fragment>{this.renderTable()}</Fragment>;
  }
}

export default SmartTable;
