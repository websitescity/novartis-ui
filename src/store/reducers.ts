import { combineReducers } from "redux";
import { Action } from "./actions";

// States' definition
export interface StoreState {
  isFetching: boolean;
  source?: Array<any>;
}

export interface State {
  source: StoreState;
}

const source = (
  state: StoreState = { isFetching: false },
  action: Action
): StoreState => {
  switch (action.type) {
    case "SET":
      return { ...state, source: action.source };
    case "SET_FETCHING":
      return { ...state, isFetching: action.isFetching };
  }
  return state;
};
export default combineReducers<State>({
  source
});
