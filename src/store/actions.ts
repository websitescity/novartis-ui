import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import Service from "../services";

// Action Definition
export interface SetAction {
  type: "SET";
  source: any[];
}
export interface SetFetcing {
  type: "SET_FETCHING";
  isFetching: boolean;
}

// Union Action Types
export type Action = SetAction | SetFetcing;

// Action Creators
export const set = (source: any[]): SetAction => {
  return { type: "SET", source };
};
export const isFetching = (isFetching: boolean): SetFetcing => {
  return { type: "SET_FETCHING", isFetching };
};

export const pullData = (
  url: string,
  limit: string
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    return new Promise<void>(resolve => {
      dispatch(isFetching(true));
      Service.getAll(url + "" + limit).then(data => {
        dispatch(set(data));
        dispatch(isFetching(false));
        resolve();
      });
    });
  };
};
