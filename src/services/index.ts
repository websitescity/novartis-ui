export default class Service {
  static getAll(url: string): Promise<any[]> {
    return new Promise((resolve, reject) => {
      fetch(url)
        .then(res => res.json())
        .then(data => {
          resolve(data.results);
        })
        .catch(function() {
          alert("Wrong URL");
        });
    });
  }
}
