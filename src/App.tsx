import React, { Component } from "react";

// Pages
import Form from "./components/form/Form";

// Assets & Style
import "./App.scss";
import "./customBootstrap.scss";

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <Form />
      </div>
    );
  }
}

export default App;
